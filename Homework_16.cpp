#include <iostream>
#include <array>
#include <ctime>

//#pragma warning(disable : 4996)

int DayofMonth()
{
    struct tm newTime;
    time_t now = time(0);
    //std::tm *ltm = localtime(&now);
    localtime_s(&newTime, &now);
    //int day = ltm->tm_mday;
    return newTime.tm_mday;
}

int main()
{
    const int N = 8;
    int array[N][N] = {};

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            array[i][j] = i + j;
            std::cout << array[i][j] << " ";
        }
        std::cout << "\n";
    }
    
    std::cout << "\n";

    int sum = 0;
    int row = DayofMonth() % N;

    for (int j = 0; j < N; j++)
    {
        sum += array[row][j];
    }
    std::cout << "Sum in row = " << sum << "\n";

    return 0;
}
